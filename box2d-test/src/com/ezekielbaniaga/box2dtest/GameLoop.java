package com.ezekielbaniaga.box2dtest;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class GameLoop extends ApplicationAdapter {

	// time step - integrator algorithm
	private static final float BOX_STEP = 1 / 60f;

	// constraint solver - velocity phase and position phase
	private static final int BOX_VELOCITY_ITERATIONS = 8;
	private static final int BOX_POSITION_ITERATIONS = 3;

	private static final float PPM = 100f; // 100 pixels per meter

	private World world;
	private Box2DDebugRenderer debugRenderer;
	private OrthographicCamera camera;

	@Override
	public void create() {
		// Setup world with gravity 9.8 meters per second
		world = new World(new Vector2(0, -9.8f), true);

		camera = new OrthographicCamera();
		camera.viewportWidth  = 480 / PPM;
		camera.viewportHeight = 320 / PPM;
		// setup camera position so that originX = bottom+left and originY = yUp
		camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0f);
		camera.update();

		// Setup Ground - Static Body
		
		// ground body 
		BodyDef groundBodyDef = new BodyDef();
		groundBodyDef.position.set(new Vector2(camera.viewportWidth / 2, 10 / PPM));
		Body groundBody = world.createBody(groundBodyDef);

		// ground shape
		PolygonShape groundBox = new PolygonShape();
		groundBox.setAsBox((camera.viewportWidth / 2f) - (0.25f/PPM) , 10.0f / PPM); //setAsBox is half width and half height. Note: minus 0.25f(so we can see the border lines)

		// ground fixture
		groundBody.createFixture(groundBox, 0.0f);

		// Setup Ball - Dynamic Body 
		//createRandomBalls();
		
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i=1; i<=400; i++) {
					try{Thread.sleep(300);}catch(Exception ex){}
                    Gdx.app.postRunnable(new Runnable() {
					
                        @Override
                        public void run() {
                            //createRowSquares((int)(Math.random()*x.length));
                        	ii++;
                        	if (ii > x.length-1) {
                        		ii=0;
                        	}
                        	createRowSquares(ii);
                        }
                    });
				}
			}
		}).start();;

		// debugger
		debugRenderer = new Box2DDebugRenderer();
	}
	
    int x[] = {30,40,50,60,70,80,90,100,110,120,130};
    int ii=0;
	private void createRowSquares(int i) {

        BodyDef sqBodyDef = new BodyDef();
        sqBodyDef.type = BodyType.DynamicBody;
        sqBodyDef.position.set((50+x[i] + 2) / PPM, 300 / PPM);
        Body body = world.createBody(sqBodyDef);
        
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(4 / PPM, 4 / PPM);
                    
        // ball fixture
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 10.0f; // 0-10kg weight
        fixtureDef.friction = 1f; // maximum friction. range is 0-1f
        fixtureDef.restitution = 0.0f; // make object bounce. range 0-1f
        body.createFixture(fixtureDef);
	}

	private void createRandomBalls() {
		for (int i=1; i<=10; i++) {
            // ball body
			BodyDef ballBodyDef = new BodyDef();
            ballBodyDef.type = BodyType.DynamicBody;
            ballBodyDef.position.set(
            	(camera.viewportWidth / 2f) + (50f * customRandom()), 
            	(camera.viewportHeight) + (50f * customRandom())); 
            Body body = world.createBody(ballBodyDef);

            // ball shape
            CircleShape circle = new CircleShape();
            circle.setRadius(5f);

            // ball fixture
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = circle;
            fixtureDef.density = 10.0f * customRandom(); // 0-10kg weight
            fixtureDef.friction = 1f; // maximum friction. range is 0-1f
            fixtureDef.restitution = 0.7f; // make object bounce. range 0-1f
            body.createFixture(fixtureDef);
		}
	}
	
	private float customRandom() {
		
		if (Math.random() > 0.5f) {
			return -((float)Math.random());
		} else {
			return (float)Math.random();
		}
	}

	@Override
	public void dispose() {
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		debugRenderer.render(world, camera.combined);
		world.step(BOX_STEP, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
