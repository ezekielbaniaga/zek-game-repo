package com.ezekielbaniaga.box2dtest;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopGame {
	
	private LwjglApplicationConfiguration config;
	
	
	public DesktopGame(String title, int width, int height, boolean fullscreen) {
		
		System.setProperty("sun.java2d.noddraw", "true"); 
        
		// config = new JoglApplicationConfiguration();
        config = new LwjglApplicationConfiguration(); 
        
        config.title = title;
        config.useGL20 = false;
        config.fullscreen = fullscreen;
        config.vSyncEnabled = true;
        config.width = width;
        config.height = height;
        config.resizable = false;
        
        //config.setFromDisplayMode(JoglApplicationConfiguration.getDesktopDisplayMode()); 
	}
	
	
	public DesktopGame(String title, int width, int height) {
		this(title, width, height, true);
	}
	
	public void start() {
		new LwjglApplication(new GameLoop(), config);
	}
}
