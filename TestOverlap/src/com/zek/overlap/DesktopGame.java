package com.zek.overlap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopGame {
	
	private LwjglApplicationConfiguration config;
	
	
	public DesktopGame(String title, int width, int height, boolean fullscreen) {
		
		System.setProperty("sun.java2d.noddraw", "true"); 
        
		// config = new JoglApplicationConfiguration();
        config = new LwjglApplicationConfiguration(); 
        
        config.title = title;
        config.useGL30 = false;
        config.fullscreen = fullscreen;
        config.vSyncEnabled = true;
        config.width = width;
        config.height = height;
        config.resizable = false;
        
        //config.setFromDisplayMode(JoglApplicationConfiguration.getDesktopDisplayMode()); 
	}
	
	
	public DesktopGame(String title, int width, int height) {
		this(title, width, height, true);
	}
	
	public void start() {
		final TO ip = new TO();

		new LwjglApplication(ip, config);

		Gdx.app.postRunnable(new Runnable() {
			
			@Override
			public void run() {
				ip.setScreen(new MainScreen());
			}
		});
	}
	
}
