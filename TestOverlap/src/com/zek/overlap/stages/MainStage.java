package com.zek.overlap.stages;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.resources.ResourceManager;

public class MainStage extends Stage {

	public MainStage() {
		Essentials e = new Essentials();
		ResourceManager rm = new ResourceManager();
		rm.setWorkingResolution("1_6");
		rm.initAllResources();

		e.rm = rm;
		
		SceneLoader loader = new SceneLoader(e);
		loader.loadScene("MainScene");
		
		addActor(loader.sceneActor);
		
	}
}
